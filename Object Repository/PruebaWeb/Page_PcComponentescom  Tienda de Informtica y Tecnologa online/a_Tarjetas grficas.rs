<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Tarjetas grficas</name>
   <tag></tag>
   <elementGuidId>7c0c65b1-fff6-4639-9400-874ed995436b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='cn_banner_hometop']/div/div/div/div[2]/section/div[2]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://cl1-api.connectif.cloud:443/content/track-click?entityId=5c91ee3f0085af29888d2196&amp;userId=5b223ad9ec0d680e78979fdf&amp;contentId=5c53fe289d856e1804e47f51&amp;contentTypeId=banner&amp;contentName=4%20articulos%20-%2006%20-%20Banner%20Categor%C3%ADas%20+%20Recomendaciones%20-%20Home%20-%20Conocidos&amp;workflowDefinitionId=5c8f9a380085af2988078b84&amp;linkUrl=https%3A%2F%2Fwww.pccomponentes.com%2Ftarjetas-graficas&amp;originalLinkUrl=https%3A%2F%2Fwww.pccomponentes.com%2Ftarjetas-graficas</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>c-family-matrix__link</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Tarjetas gráficas</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Tarjetas gráficas
        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;cn_banner_hometop&quot;)/div[@class=&quot;cn__content-container&quot;]/div[@class=&quot;cn__block&quot;]/div[@class=&quot;cn__cell cn__width-100 cn__responsive-width-100&quot;]/div[@class=&quot;cn__element&quot;]/section[@class=&quot;c-family-matrix&quot;]/div[@class=&quot;c-family-matrix__card&quot;]/a[@class=&quot;c-family-matrix__link&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='cn_banner_hometop']/div/div/div/div[2]/section/div[2]/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PcCom'])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MÁS RELEVANTES'])[1]/following::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Smartphones'])[3]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <value>//a[contains(@href, 'https://cl1-api.connectif.cloud:443/content/track-click?entityId=5c91ee3f0085af29888d2196&amp;userId=5b223ad9ec0d680e78979fdf&amp;contentId=5c53fe289d856e1804e47f51&amp;contentTypeId=banner&amp;contentName=4%20articulos%20-%2006%20-%20Banner%20Categor%C3%ADas%20+%20Recomendaciones%20-%20Home%20-%20Conocidos&amp;workflowDefinitionId=5c8f9a380085af2988078b84&amp;linkUrl=https%3A%2F%2Fwww.pccomponentes.com%2Ftarjetas-graficas&amp;originalLinkUrl=https%3A%2F%2Fwww.pccomponentes.com%2Ftarjetas-graficas')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//section/div[2]/a</value>
   </webElementXpaths>
</WebElementEntity>
