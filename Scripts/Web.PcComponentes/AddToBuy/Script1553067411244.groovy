import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://www.pccomponentes.com/')

WebUI.click(findTestObject('PruebaWeb/Page_PcComponentescom  Tienda de Informtica y Tecnologa online/a_Tarjetas grficas'))

WebUI.click(findTestObject('PruebaWeb/Page_Tarjetas Grficas Nvidia y AMD  PcComponentes/a_(186)_GTM-productClick enlace-superpuesto'))

WebUI.click(findTestObject('PruebaWeb/Page_Gigabyte Radeon RX 580 Gaming 8G 8GB GDDR5/strong_Comprar'))

WebUI.click(findTestObject('PruebaWeb/Page_Detalle de mi carrito  PcComponentes  Pccomponentes/a_REALIZAR PEDIDO'))

WebUI.closeBrowser()

